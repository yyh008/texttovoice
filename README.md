# java文字转语音播报功能

#### 介绍
java文字转语音播报功能

#### 使用说明

1.  下载压缩文件，解压，把jacob-1.18-x64.dll文件复制到jdk安装位置的bin目录下
（.dll文字github下载地址：https://github.com/freemansoft/jacob-project/releases）
2.  ![输入图片说明](https://images.gitee.com/uploads/images/2022/0708/110456_ddee5e73_5393820.jpeg "1.jpg")
3.  修改需要播报的文字
